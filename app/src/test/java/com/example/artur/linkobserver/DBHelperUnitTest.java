package com.example.artur.linkobserver;
import android.content.Context;
import android.os.Environment;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;

import java.io.File;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class DBHelperUnitTest {
    @Rule
    public TemporaryFolder storageDirectory = new TemporaryFolder();
    private DBHelper dbHelper;
    private ManageLinksActivity mng;
    private File nonExistentDirectory;
    private File existentDirectory;
    private Context context;



    @Mock
    Context mMockContext;

    private String url="http://interia.pl";
    @Before
    public void setUp() {


        nonExistentDirectory = Mockito.mock(File.class);
        Mockito.when(nonExistentDirectory.exists()).thenReturn(false);
        existentDirectory = storageDirectory.getRoot();
        /*Jest problem bo kontruktor klasy dbhelper potrzebuje kontekstu
        Bez kontekstu nie zadziala i reszta testow tez sie wykrzaczy
        rozwiazanie jakie znalazlem to androix lub roboelectric
         */


        /*
        Testy powinny byc dobrze napchane trescia tylko nie ma jak odpalic
        dlatego sa wylaczone
         */

    }

    @PrepareForTest({Environment.class})


    public void DBHelperAddLink()
    {

        dbHelper.addLink(url);
        ArrayList<String>urle=dbHelper.getUrls();

        int id_url=urle.indexOf(url);
        int unexpected_id=-1;
        assertNotEquals(unexpected_id,id_url);
    }

    public void DBHelperDeleteLink()
    {
        dbHelper.addLink(url);
        ArrayList<String>urle=dbHelper.getUrls();
        dbHelper.deleteLink(url);
        int id_link=urle.indexOf(url);
        assertEquals(id_link,-1);
    }

    public void DBHelperUpdateHash()
    {
        String local_url="up.krakow.pl";
        String Hash="MOaRddfQXcAaOIJa/SZ35OZNgOu6eqTf";
        dbHelper.addLink(local_url);
        dbHelper.updateHash(Hash,local_url);
        String getHash=dbHelper.getHash(local_url);
        assertEquals(Hash,getHash);
    }

    public void DBHelperGetAllRows()
    {
        String local_url="intranet.krakow.pl";
        String Hash="MOaRddfQXcAaOIJa/SZ35OZNgOu6eqTf";
        dbHelper.addLink(local_url);
        dbHelper.updateHash(Hash,local_url);
        ArrayList <String>rekordy=dbHelper.getAllRows();
        assertNotNull(rekordy);

    }




}
