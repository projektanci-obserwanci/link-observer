package com.example.artur.linkobserver;

import android.content.Context;
import android.os.Environment;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertNotNull;

//@RunWith(MockitoJUnitRunner.class)
@RunWith(PowerMockRunner.class)
public class PamiecUnitTest {

    @Rule
    public TemporaryFolder storageDirectory = new TemporaryFolder();
    private Pobieracz pobieracz;
    private File nonExistentDirectory;
    private File existentDirectory;

    @Mock
    Context mMockContext;

    @Before
    public void setup() {
        nonExistentDirectory = Mockito.mock(File.class);
        Mockito.when(nonExistentDirectory.exists()).thenReturn(false);

        existentDirectory = storageDirectory.getRoot();
        this.pobieracz = new Pobieracz(existentDirectory.getAbsolutePath());
        PowerMockito.mockStatic(Environment.class);
    }

    @PrepareForTest({Environment.class})
    @Test
    public void test_mozliwosc_uzywania_pamieci_telefonu_do_zapisywania_plikow() {
        Mockito.when(Environment.getExternalStorageState()).thenReturn(Environment.MEDIA_MOUNTED);
        Mockito.when(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)).thenReturn(existentDirectory);
        String directory = "/basic";
        String fileName = "index.html";
        try {
            pobieracz.zapisz_plik(mMockContext,directory,fileName,"<body></body>");

        } catch (IOException e){
        }
        int czy_istnieje=pobieracz.czy_plik(directory,fileName);
        assertNotNull(czy_istnieje);
    }

}
