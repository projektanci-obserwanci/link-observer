package com.example.artur.linkobserver;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.core.classloader.annotations.PrepareForTest;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PobieraczUnitTest {

    @Rule
    public TemporaryFolder storageDirectory = new TemporaryFolder();
    private Pobieracz pobieracz;
    private File nonExistentDirectory;
    private File existentDirectory;
    private String haslo="/storage/emulated/0/LinkObserver/interia.pl/tmp.html";

    @Mock
    Context mMockContext;

    @Before
    public void setUp() {
        nonExistentDirectory = Mockito.mock(File.class);
        Mockito.when(nonExistentDirectory.exists()).thenReturn(false);
        existentDirectory = storageDirectory.getRoot();
        this.pobieracz = new Pobieracz(existentDirectory.getAbsolutePath());
    }

    @PrepareForTest({Environment.class})
    @Test
    public void pobieraczIsNotNull() {
        assertNotNull(this.pobieracz);
    }

    @Test
    public void pobieraczDawajHash() {
        try {
            String hash=Pobieracz.dawajHash(haslo);
            assertThat(isValidMD5(hash), is(true));
        }catch (IOException e){
            System.out.println(e.getMessage());
        } catch (NoSuchAlgorithmException e){
            System.out.println(e.getMessage());
        }
    }
    @Test
    public void pobieraczCzyZawiera()
    {
        int oczekiwane=0;
        int notOczekiwane=-1;
        String spacja=" ";
        String tabulator="\t";
        String nowaLinia="\n";
        String przecinek=",";
        String kropka=".";
        String puste="";
        String inny="malpa";
        Assert.assertEquals(oczekiwane,pobieracz.czyZawiera(spacja));
        Assert.assertEquals(oczekiwane,pobieracz.czyZawiera(tabulator));
        Assert.assertEquals(oczekiwane,pobieracz.czyZawiera(nowaLinia));
        Assert.assertEquals(oczekiwane,pobieracz.czyZawiera(przecinek));
        Assert.assertEquals(oczekiwane,pobieracz.czyZawiera(kropka));
        Assert.assertEquals(oczekiwane,pobieracz.czyZawiera(puste));
        Assert.assertEquals(notOczekiwane,pobieracz.czyZawiera(inny));


    }
    @Test
    public void pobieraczUsunZnaki()
    {

        String napisPrzecinek="Kot,";
        String napisKropka="Kot.";
        String oczekiwany="Kot";
        Assert.assertEquals(oczekiwany,pobieracz.usunZnaki(napisPrzecinek));
        Assert.assertEquals(oczekiwany,pobieracz.usunZnaki(napisKropka));


    }
    @Test
    public void pobieraczCzyKatalog()
    {
        String katalog="ToJestPlik";
        int expected=0;
        int czy_katalog=pobieracz.czy_katalog(katalog);
        Assert.assertEquals(expected,czy_katalog);
    }



    public boolean isValidMD5(String s) {
        return s.matches("^[a-fA-F0-9]{32}$");
    }

}
