package com.example.artur.linkobserver;

import android.content.Context;
import android.os.Environment;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;

import java.io.File;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ObserwatorUnitTest {
    @Rule
    public TemporaryFolder storageDirectory = new TemporaryFolder();
    private Pobieracz pobieracz;
    private File nonExistentDirectory;
    private File existentDirectory;

    @Mock
    Context mMockContext;

    private Obserwator obserwator;
    private String url="http://interia.pl";
    @Before
    public void setUp() {

        nonExistentDirectory = Mockito.mock(File.class);
        Mockito.when(nonExistentDirectory.exists()).thenReturn(false);

        existentDirectory = storageDirectory.getRoot();
        this.pobieracz = new Pobieracz(existentDirectory.getAbsolutePath());
        this.obserwator = new Obserwator(this.pobieracz);
    }

    @PrepareForTest({Environment.class})
    @Test
    public void obserwatorIsNotNull() {
        assertNotNull(this.obserwator);
    }

    @Test
    public void obserwatorZwrocUrl()
    {
        String url_local="up.krakow.pl";
        obserwator.dodaj_link(url_local);
        ArrayList<String> urle= obserwator.zwrocUrle();
        assertEquals(urle.contains(url_local),true);
        obserwator.usun_link(0);
    }

    @Test
    public void obserwatorDodajLink(){
        obserwator.dodaj_link(url);

        ArrayList<String> urle= obserwator.zwrocUrle();
        assertEquals(urle.get(0),url);
    }
    @Test
    public void obserwatorUsunLink()
    {

        obserwator.dodaj_link(url);
        ArrayList<String> urle= obserwator.zwrocUrle();
        int id_link=urle.indexOf(url);
        obserwator.usun_link(id_link);
        urle= obserwator.zwrocUrle();
        assertEquals(urle.indexOf(url),-1);

    }

    @Test
    public void obserwatorZwrocKatalog()
    {
        String return_url=obserwator.zwroc_katalog(url);
        String correct_url="LinkObserver/interia.pl";
        assertEquals(return_url,correct_url);
    }
    @Test
    public void obserwatorZwrocPlik()
    {


        String return_name=obserwator.zwroc_plik(url);
        String correct_name="index.html";
        assertEquals(return_name,correct_name);

        String link_with_name="http://onet.pl/strona.html";
        String return_name2=obserwator.zwroc_plik(link_with_name);
       assertEquals(correct_name,return_name2);

    }




}
