package com.example.artur.linkobserver;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import org.jsoup.Jsoup;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.jar.Attributes;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by root on 24.03.2018.
 */

public class Pobieracz {
    public static String KatalogProjektu="LinkObserver";
    public String root;

    public Pobieracz(){
        root=Environment.getExternalStorageDirectory().toString();
    }

    public Pobieracz(String my_root){
        root=my_root;
    }
    private void Pobierz(){

    }
    public static String html2text(String _response)
    {
        return Jsoup.parse(_response).text();
    }
    public static String sprawdz_zmiana(Context context,String url, String bufor,DBHelper dbHelper) throws IOException,NoSuchAlgorithmException{
        String plik_tmp="tmp.html";
        String porownanie=new String();
        int istnieje_plik=-1;
        boolean rename_status;
        Log.i("PRE","zwroty");
        String katalog=Obserwator.zwroc_katalog(url);
        String nazwa_plik=Obserwator.zwroc_plik(url);
        Log.d("katalog",katalog);
        Log.d("nazwa_plik",nazwa_plik);
        Pobieracz.utworz_katalog(katalog);
        Pobieracz pobieracz=new Pobieracz();
        istnieje_plik=pobieracz.czy_plik(katalog,nazwa_plik);
        Log.d("ISTNIEJE_PLIK",""+istnieje_plik);
        if(istnieje_plik==-1){
            pobieracz.zapisz_plik(context,katalog,nazwa_plik,bufor);
        } else {
            pobieracz.zapisz_plik(context,katalog,plik_tmp,bufor);
            Log.d("PRECOMP","tak");
            porownanie=pobieracz.porownaj_pliki(katalog,nazwa_plik,plik_tmp,dbHelper,url);
            Log.d("POSTCOMP",""+porownanie);

        }

        if(!porownanie.isEmpty()) {

            File oldFolder = new File(Environment.getExternalStorageDirectory(),katalog+"/"+plik_tmp);
            File newFolder = new File(Environment.getExternalStorageDirectory(),katalog+"/"+nazwa_plik);
            rename_status=oldFolder.renameTo(newFolder);
            if(rename_status)
            {
                Log.d("RENAME_F: Y ","state: "+rename_status);
                return "tak";
            }
            else
            {
                Log.d("RENAME_F: N ","state: "+rename_status);
                return "nie";
            }

        } else {
            Log.d("PREDELETE"," y");
            pobieracz.usun_zawartosc(katalog,plik_tmp);
            return "nie";
        }
    }

    public void usun_zawartosc(String sciezka, String plik) {
        int isDir=czy_katalog(sciezka);
        int isFile=czy_plik(sciezka,plik);

       // Log.d("DEL_CONT: ","czy_katalog: "+isDir+" czy_plik: "+isFile+"\n");
        if(isFile==1 && !plik.contentEquals("nic")) {
            String[] dirs=sciezka.split("/");
            File file = new File(this.root + "/"+KatalogProjektu+"/"+dirs[1],plik);
            Log.d("TODELETE",file.getAbsolutePath());
            boolean deleted = file.delete();
        }
        if(plik.contentEquals("nic") && isDir==1) {
            File fileDir = new File(sciezka);

            String[] files = fileDir.list();
            for (int i = 0; i < files.length; i++) {
                File file = new File(sciezka+"/"+files[i]);
                boolean deleted = file.delete();
                //Log.d("TODELETE",file.getAbsolutePath());
            }
            File file = new File(sciezka);
            boolean deleted = file.delete();
           // Log.d("TODELETE",file.getAbsolutePath());
        }
    }

    public static void utworz_katalog(String katalog){
        File folder = new File(Environment.getExternalStorageDirectory()+ "/"+katalog);
        Log.d("Make dir: ","Path:= "+folder.getAbsolutePath()+"\n");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }

    }

    protected static int czy_katalog(String katalog){
      //  File f = new File(Environment.getExternalStorageDirectory() + "/"+katalog);
        File f = new File(katalog);

        if(f.isDirectory())
        {
            Log.d("ISDIR: ",f.isDirectory()+" FULL PATH: "+f+"\n");
            return 1;
        }

        else
        {
            Log.d("ISDIR: ",f.isDirectory()+" FULL PATH: "+f+"\n");
            return 0;
        }

    }

    public int czy_plik(String katalog, String plik){
        String[] dirs=katalog.split("/");
        File file = new File( this.root + "/"+KatalogProjektu+"/"+dirs[1],plik);
        if(file.exists())
        {
            Log.d("FILE_IS","return_val "+1);
            return 1;
        }
        else
        {
            Log.d("FILE_ISNOT","return_val "+"-"+1);
            return -1;
        }
    }

    public void zapisz_plik(Context mcoContext, String dir, String sFileName, String sBody) throws FileNotFoundException, IOException{
        try{
            String[] dirs=dir.split("/");
            File myDir = new File(this.root + "/"+KatalogProjektu+"/"+dirs[1]);
            myDir.mkdirs();

            Log.d("DIR_E",""+myDir.exists());
            File file = new File(root + "/"+KatalogProjektu+"/"+dirs[1], sFileName);
            file.createNewFile();
            Log.d("FIL_E",""+file.exists());
            FileOutputStream os = new FileOutputStream(file);
            os.write(sBody.getBytes());
            os.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static int czyZawiera(String _napis)
    {
        if(_napis.equals("\n"))
            return 0;
        if(_napis.equals("\t"))
            return 0;
        if(_napis.equals(" "))
            return 0;
        if(_napis.equals(""))
            return 0;
        if(_napis.equals(","))
            return 0;
        if(_napis.equals("."))
            return 0;
        if(_napis.isEmpty())
            return 0;
        return -1;

    }
    public static String usunZnaki(String _napis)
    {
        if(!(_napis.equals(",")) ||  !(_napis.equals(".")))
        {
            if(_napis.contains(",") ||_napis.contains(".")  )
            {
                StringBuilder tmp = new StringBuilder(_napis);
                tmp.deleteCharAt(_napis.length()-1);
                _napis=tmp.toString();
            }
        }


        return _napis;
    }
    public String porownaj_pliki(String katalog,String _old,String _new,DBHelper dbhelper,String url) throws IOException, FileNotFoundException, NoSuchAlgorithmException
    {
        String[] dirs=katalog.split("/");
        String _oldHash;
        boolean czyPierwszyRaz=false;
        StringBuilder zwrotka=new StringBuilder();
        //Log.d("HA",this.root + "/"+KatalogProjektu+"/"+dirs[1]+"/"+_new);
        if(dbhelper.getHash(url)==null)
        {
            Log.d("HASH_DB","BRAK HASHA dla url:\t"+url);
            czyPierwszyRaz=true;
            _oldHash=dawajHash(this.root + "/"+KatalogProjektu+"/"+dirs[1]+"/"+_old);
            dbhelper.updateHash(_oldHash, url);
        }
        else
        {
            _oldHash=dbhelper.getHash(url);
            Log.d("HASH_DB_else","Znaleziony hash: "+_oldHash+" dla url: "+url);

        }

        int lineNumber=0;
        int wordCount=0;
        String left,right;
        String _leftWord[] ,_rightWord[] ;
        String _left,_right;

        if(!czyPierwszyRaz) {

            String _newHash=dawajHash(this.root + "/"+KatalogProjektu+"/"+dirs[1]+"/"+_new);

            if (!_oldHash.equals(_newHash)) {
                Log.d("HASH_NOEQ", "old: " + _oldHash + " new: " + _newHash + "\n");


                File _oldFile = new File(root + "/" + KatalogProjektu + "/" + dirs[1], _old);
                File _newFile = new File(root + "/" + KatalogProjektu + "/" + dirs[1], _new);
                Scanner _oldRead = new Scanner(_oldFile);
                Scanner _newRead = new Scanner(_newFile);


                while ((_oldRead.hasNextLine()) && (_newRead.hasNextLine())) {

                    left = html2text(_oldRead.nextLine());
                    right = html2text(_newRead.nextLine());
                    lineNumber++;
                    Log.d("DIFF_LINE", "in: " + lineNumber);

                    if (!left.equals(right) && czyZawiera(right) == -1 && czyZawiera(left) == -1) {

                        _leftWord = left.split(" ");
                        _rightWord = right.split(" ");
                        if (_leftWord.length == _rightWord.length) {
                            for (int i = 0; i < _leftWord.length; i++) {
                                _left = usunZnaki(_leftWord[i]);
                                _right = usunZnaki(_rightWord[i]);
                                if (czyZawiera(_leftWord[i]) == -1 && czyZawiera(_rightWord[i]) == -1) {
                                    if (!_leftWord[i].equals(_rightWord[i]) && !_left.equals(_right)) {

                                        wordCount++;
                                        zwrotka.append("Linia: " + lineNumber + ", slowo: " + wordCount + ":" + _rightWord[i] + "\n");
                                        Log.d("DIFF_WORD", "in word no." + wordCount + ":" + _rightWord[i] + "\n");
                                    }
                                }
                            }


                        }
                        if (_leftWord.length < _rightWord.length) {
                            for (int i = 0; i < _leftWord.length; i++) {
                                _left = usunZnaki(_leftWord[i]);
                                _right = usunZnaki(_rightWord[i]);
                                if (czyZawiera(_leftWord[i]) == -1 && czyZawiera(_rightWord[i]) == -1) {
                                    if (!_leftWord[i].equals(_rightWord[i]) && !_left.equals(_right)) {

                                        wordCount++;
                                        zwrotka.append("Linia: " + lineNumber + ", slowo: " + wordCount + ":" + _rightWord[i] + "\n");
                                        Log.d("DIFF_WORD", "in word no." + wordCount + ":" + _rightWord[i] + "\n");

                                    }
                                }
                            }


                            for (int i = _leftWord.length; i < _rightWord.length; i++) {
                                _right = usunZnaki(_rightWord[i]);
                                if (czyZawiera(_rightWord[i]) == -1 && _right.equals(_rightWord[i])) {
                                    wordCount++;
                                    zwrotka.append("Linia: " + lineNumber + ", slowo: " + wordCount + ":" + _rightWord[i] + "\n");
                                    Log.d("DIFF_WORD", "in word no." + wordCount + ":" + _rightWord[i] + "\n");
                                }

                            }

                        }


                        if (_leftWord.length > _rightWord.length) {

                            for (int i = 0; i < _rightWord.length; i++) {
                                _left = usunZnaki(_leftWord[i]);
                                _right = usunZnaki(_rightWord[i]);
                                if (czyZawiera(_leftWord[i]) == -1 && czyZawiera(_rightWord[i]) == -1) {
                                    if (!_leftWord[i].equals(_rightWord[i]) && !_left.equals(_right)) {

                                        wordCount++;
                                        zwrotka.append("Linia: " + lineNumber + ", slowo: " + wordCount + ":" + _leftWord[i] + "\n");
                                        Log.d("DIFF_WORD", "in word no." + wordCount + ":" + _leftWord[i] + "\n");
                                    }
                                }

                            }


                            for (int i = _rightWord.length; i < _leftWord.length; i++) {
                                _left = usunZnaki(_leftWord[i]);
                                if (czyZawiera(_leftWord[i]) == -1 && _left.equals(_leftWord[i])) {
                                    wordCount++;
                                    zwrotka.append("Linia: " + lineNumber + ", slowo: " + wordCount + ":" + _leftWord[i] + "\n");
                                    Log.d("DIFF_WORD", "in word no." + wordCount + ":" + _leftWord[i] + "\n");
                                }

                            }
                        }

                    }
                    wordCount = 0;


                }
                if (_newFile.length() > _oldFile.length()) {
                    while (_newRead.hasNextLine()) {
                        left = html2text(_newRead.nextLine());
                        lineNumber++;
                        int isContain = czyZawiera(left);
                        if (isContain == -1) {
                            _leftWord = left.split(" ");

                            for (int i = 0; i < _leftWord.length; i++) {
                                if (czyZawiera(_leftWord[i]) == -1) {
                                    wordCount++;
                                    zwrotka.append("Linia: " + lineNumber + ", slowo: " + wordCount + ":" + _leftWord[i] + "\n");
                                    // Log.d("DIFF_WORD_old", "in word no." + wordCount + ":" + _leftWord[i] + "\n");

                                }
                            }

                        }
                        wordCount = 0;
                    }
                } else if (_oldFile.length() > _newFile.length()) {
                    while (_oldRead.hasNextLine()) {
                        left = html2text(_oldRead.nextLine());
                        lineNumber++;
                        int isContain = czyZawiera(left);
                        if (isContain == -1) {
                            _leftWord = left.split(" ");
                            for (int i = 0; i < _leftWord.length; i++) {
                                if (czyZawiera(_leftWord[i]) == -1) {
                                    wordCount++;
                                    zwrotka.append("Linia: " + lineNumber + ", slowo: " + wordCount + ":" + _leftWord[i] + "\n");
                                    // Log.d("DIFF_WORD_old", "in word no." + wordCount + ":" + _leftWord[i] + "\n");

                                }
                            }

                        }
                        wordCount = 0;
                    }
                }
                _newRead.close();
                _oldRead.close();
                if (!zwrotka.toString().isEmpty()) {
                    dbhelper.updateHash(_newHash, url);
                }

            } else {
                Log.d("HASH_EQ ", "old: " + _oldHash + " new: " + _newHash + "\n");
                Log.d("DIFF_NULL", zwrotka.toString());
                zwrotka.append("");
            }

        }

        return zwrotka.toString();
    }

     /*
        @author: Michał Hudaszek

        funkcja dawajHash została zapożyczona niekomercyjnie ze strony: 'https://www.mkyong.com/java/java-sha-hashing-example/'
        i po przeróbkach wykorzystana w tym projekcie
     */

    public static String dawajHash(String fname) throws FileNotFoundException, IOException, NoSuchAlgorithmException
    {
        MessageDigest digest = MessageDigest.getInstance("MD5");
        FileInputStream fis=new FileInputStream(fname);
        int bufor_size=16384;
        byte[] bufor=new byte[bufor_size];
        int licznik=0;
        while((licznik=fis.read(bufor))!=-1) {
            digest.update(bufor, 0, licznik);
        }
        fis.close();
        byte[]buforDiggest=digest.digest();
        StringBuilder hash=new StringBuilder();
        for (int i=0;i<buforDiggest.length;i++) {
            hash.append(Integer.toString((buforDiggest[i] & 0xff) + 0x100, 16).substring(1));
        }
        return hash.toString();
    }
}
