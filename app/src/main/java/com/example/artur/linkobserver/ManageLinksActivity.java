package com.example.artur.linkobserver;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Debug;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebViewFragment;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class ManageLinksActivity extends Activity {

    protected EditText et;
    protected Button addLinkBtn, removeLinkBtn;
    protected Switch observeLinkSwt;
    protected ListView linksListView;
    protected Obserwator obserwator;
    protected ArrayAdapter<String> adapter;
    private DBHelper dbhelper;
    private final AtomicInteger c = new AtomicInteger(0);

    public static int DIALOG=1;

    WeakHashMap< Integer, Dialog > managedDialogs = new WeakHashMap< Integer, Dialog>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.link_manager);

        obserwator=new Obserwator();
        dbhelper=new DBHelper(this);
        obserwator.importUrls(dbhelper);
        addLinkBtn = (Button) findViewById(R.id.addLinkBtn);
        //removeLinkBtn = (Button) findViewById(R.id.removeLinkBtn);
        observeLinkSwt=(Switch) findViewById(R.id.obserwe_on_off);
        et = (EditText) findViewById(R.id.et);
        linksListView = (ListView) findViewById(R.id.linksListView);

        /*

        Ustawienia dla adaptera t.j. listy linków. Ustawiony kolor każdego linka z listy w zarządzaniu linkami na czarny

         */

        adapter=new ArrayAdapter<String>(getApplicationContext(), R.layout.list_item_white, R.id.list_content, obserwator.zwrocUrle()){
            @Override
            public View getView(int position,View convertView, ViewGroup parent){
                View view = super.getView(position,convertView, parent);

                TextView text= (TextView) view.findViewById(R.id.list_content);
                text.setTextColor(Color.WHITE);
                return view;
            }
        };

        /*
            Aktualizacja linków na liście
         */

        linksListView.setAdapter(adapter);
        addLinkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean duplikat=false;
                for(String url:obserwator.zwrocUrle()){
                    if(url.equals(et.getText().toString()) && !(et.getText().toString()).isEmpty())
                        duplikat=true;
                }
                if(!(et.getText().toString()).isEmpty()) {


                    if (duplikat) {
                        Toast.makeText(getApplicationContext(), "Już jest taki link na liście ! Podaj inny.", Toast.LENGTH_LONG).show();
                    } else {
                        obserwator.dodaj_link(et.getText().toString());
                        dbhelper.addLink(et.getText().toString());
                    }
                }
                ((ArrayAdapter)adapter).notifyDataSetChanged();
                et.setText("");
            }
        });

        /*

        Ustawione przesuwanie palców po linkach gdyby nie mieściły by się na ekranie

         */

        linksListView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        /*
        Edycja linka
         */
        linksListView.setId(R.id.linksListView);
        linksListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                /*
                Uruchomienie MessageBoxa do zarządzania linkiem
                 */
                final AlertDialog.Builder mBuilder = new AlertDialog.Builder(ManageLinksActivity.this);

                View mView = getLayoutInflater().inflate(R.layout.dialog_link, null);
                TextView tv=mView.findViewById(R.id.url);

                final String selectedFromList = adapterView.getItemAtPosition(index).toString();
                tv.setText(selectedFromList);
                final String url=selectedFromList;
                Button btn1 = (Button) mView.findViewById(R.id.btndialoglink);
                Button btn2 = (Button) mView.findViewById(R.id.btndialogbrowser);
                mBuilder.setView(mView);
                final AlertDialog dialog = mBuilder.create();
                managedDialogs.put(DIALOG, dialog);

                final int indeks=index;
                dialog.show();
                btn1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        obserwator.usun_link(indeks);
                        dbhelper.deleteLink(url);
                        ((ArrayAdapter)adapter).notifyDataSetChanged();
                        dialog.dismiss();
                    }
                });

                btn2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        dialog.dismiss();

                        Intent intent = new Intent(getApplicationContext(), WebViewActivity.class);
                        intent.putExtra("url", selectedFromList);
                        Log.e("url", selectedFromList);
                        startActivity(intent);
                    }
                });
            }
        });
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if(prefs.getBoolean("is_checked",true)){
            SharedPreferences prefs2 = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

            String frequency=prefs2.getString("time_cycle","DEFAULT");
            SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("user-preferences",Context.MODE_PRIVATE);
            boolean is_true_user_title_times=prefs2.getBoolean(Integer.toString(R.string.switch_title_times),false);
            int wait_time;
            if(is_true_user_title_times){
                int hour=sharedPref.getInt(Integer.toString(R.string.hour_title_times),1);
                int minute=sharedPref.getInt(Integer.toString(R.string.minute_title_times),1);
                int second=sharedPref.getInt(Integer.toString(R.string.second_title_times),1);
                wait_time=get_user_interval_time(hour,minute,second);
            } else {
                wait_time=get_interval_time(frequency);
            }

            Intent notificationIntent = new Intent(ManageLinksActivity.this, PrzegladUsluga.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(ManageLinksActivity.this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Calendar cal = Calendar.getInstance();
            if (cal.get(Calendar.SECOND) >= 30)
                cal.add(Calendar.MINUTE, 1);
            cal.set(Calendar.SECOND, 30);
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            try {

                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,cal.getTimeInMillis(),wait_time,pendingIntent);
                //    prefs.edit().putBoolean("is_checked",true).apply();// .getString("time_cycle","DEFAULT");
            } catch (NullPointerException e){
                Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
            }
            observeLinkSwt.setChecked(true);
        }
        observeLinkSwt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String frequency=prefs.getString("time_cycle","DEFAULT");
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("user-preferences",Context.MODE_PRIVATE);
                boolean is_true_user_title_times=sharedPref.getBoolean(Integer.toString(R.string.switch_title_times),false);
                int wait_time;
                if(is_true_user_title_times){
                    int hour=sharedPref.getInt(Integer.toString(R.string.hour_title_times),1);
                    int minute=sharedPref.getInt(Integer.toString(R.string.minute_title_times),1);
                    int second=sharedPref.getInt(Integer.toString(R.string.second_title_times),1);
                    wait_time=get_user_interval_time(hour,minute,second);
                    Log.i("Czasek=>",Integer.toString(wait_time));
                } else {
                    wait_time=get_interval_time(frequency);
                }

                Intent notificationIntent = new Intent(ManageLinksActivity.this, PrzegladUsluga.class);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(ManageLinksActivity.this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                Calendar cal = Calendar.getInstance();
                if (cal.get(Calendar.SECOND) >= 30)
                    cal.add(Calendar.MINUTE, 1);
                cal.set(Calendar.SECOND, 30);

                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                try {
                    if(isChecked){
                        Toast.makeText(getApplicationContext(),"Linki są obserwowane",Toast.LENGTH_SHORT).show();
                        Log.i("Czasek2=>",Integer.toString(wait_time));
                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,cal.getTimeInMillis(),wait_time,pendingIntent);
                        prefs.edit().putBoolean("is_checked",true).apply();// .getString("time_cycle","DEFAULT");
                    } else {
                        alarmManager.cancel(pendingIntent);
                        prefs.edit().putBoolean("is_checked",false).apply();// .getString("time_cycle","DEFAULT");

                    }
                } catch (NullPointerException e){
                    Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public int get_user_interval_time(int hour,int minute,int second){
        int time=0;
        if(hour==0 && second==0) time=(minute*60);
        else if(hour==0 && minute==0) time=second;
        else if(minute==0 && second==0) time=hour*60*60;
        else time=second+(minute*60)+(hour*60*60);
        return time*1000;
    }

    public int get_interval_time(String frequency){
        int wait_time=10000;
        int one_minute=60000;
        switch (frequency){
            case "Co 30 sekund":
                wait_time=one_minute/2;
                break;
            case "Co 2 minuty":
                wait_time=2*one_minute;
                break;
            case "Co godzinę":
                wait_time=60*one_minute;
                break;
            case "Co 2 godziny":
                wait_time=2*60*one_minute;
                break;
            case "Co 6 godzin":
                wait_time=6*60*one_minute;
                break;
            case "Co 12 godzin":
                wait_time=12*60*one_minute;
                break;
            case "Każdego dnia":
                wait_time=24*60*one_minute;
                break;
            case "Co 2 dni":
                wait_time=2*24*60*one_minute;
                break;
        }
        return wait_time;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        obserwator.exportUrls(dbhelper);
    }
}