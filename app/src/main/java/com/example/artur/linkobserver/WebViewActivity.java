package com.example.artur.linkobserver;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewActivity extends Activity {

    WebView webView;

    private static String webPrefix = "http://";

//    @RequiresApi(api = Build.VERSION_CODES.ECLAIR_MR1)
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_link);

        webView = (WebView) findViewById(R.id.webView);
        webView.setWebViewClient(new WebViewClient());
        webView.clearCache(true);


        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        String url = getIntent().getStringExtra("url");
        Log.e("url", url);
        url = addHTTPIfThereIsNot(url);
        Log.e("url", url);
        webView.loadUrl(url);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    protected String addHTTPIfThereIsNot(String url) {

        if(!url.startsWith("http")) {
            url = webPrefix + url;
        }
        return url;
    }
}
