package com.example.artur.linkobserver;


import android.util.Log;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Obserwator
{
    private ArrayList<String> url;
    private Pobieracz pobieracz;

    public Obserwator(Pobieracz inny_pobieracz) {
        url=new ArrayList<>();
        pobieracz=inny_pobieracz;
        pobieracz.utworz_katalog("LinkObserver");
        String sciezka=pobieracz.root+"/"+pobieracz.KatalogProjektu;
        int isCreated=pobieracz.czy_katalog(sciezka);
        if(isCreated==1) {
            Log.d("MKDIR: Y ","did I create ? "+isCreated+"\n");
        } else {
            Log.d("MKDIR: N "," I did not create  "+isCreated+"\n");
        }
    }

    public Obserwator() {
        url=new ArrayList<>();
        pobieracz=new Pobieracz();
        pobieracz.utworz_katalog("LinkObserver");
        String sciezka=pobieracz.root+"/"+pobieracz.KatalogProjektu;
        int isCreated=pobieracz.czy_katalog(sciezka);
        if(isCreated==1) {
            Log.d("MKDIR: Y ","did I create ? "+isCreated+"\n");
        } else {
            Log.d("MKDIR: N "," I did not create  "+isCreated+"\n");
        }
    }


    public void obserwuj()
    {

    }

    public void wyswietl()
    {

    }

    public void zakoncz()
    {

    }

    public ArrayList<String> zwrocUrle()
    {
        return this.url;
    }

    public void dodaj_link(String url)
    {
        if(!(url.isEmpty()))
        {
            this.url.add(url);
        }

    }


    public void usun_link(int id)
    {
        String _url=this.url.get(id);
        this.url.remove(id);
        try
        {
            String tmp=pobieracz.root+"/"+zwroc_katalog(_url);
            Log.i("Chce usunąć: ",tmp);
            pobieracz.usun_zawartosc(tmp,"nic");

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

    }
   

    protected void exportUrls(DBHelper dbhelper)
    {
        dbhelper.clearTable();
        for(int i=0;i<url.size();i++)
        {
            dbhelper.addLink(url.get(i));
        }
    }

    protected void importUrls(DBHelper dbhelper)
    {
        url.clear();
        url=dbhelper.getUrls();
    }

    public void powiadomZmiana(String url)
    {

    }

    public static String zwroc_plik(String link)
    {
        String nazwa_pliku;
        int pozycja=link.lastIndexOf("/");
        if(pozycja==-1) {
            nazwa_pliku="index.html";
            return nazwa_pliku;
        }
        nazwa_pliku=link.substring(link.lastIndexOf("/")+1);
        if(nazwa_pliku.contains(""))
            nazwa_pliku="index.html";
        return nazwa_pliku;
    }

    public static String zwroc_katalog(String link)
    {
        String katalog="";
        List<String> allMatches = new ArrayList<String>();
        Matcher m = Pattern.compile("^(http://|www.|https://|)(www.)?([a-zA-Z0-9.]+)").matcher(link);
        while (m.find()) {
            allMatches.add(m.group(3));
        }
        if(!allMatches.isEmpty()){
            katalog=allMatches.toArray(new String[0])[0];
        }
        katalog ="LinkObserver/"+katalog;
        return katalog;
    }
}
