package com.example.artur.linkobserver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;


public class PrzegladUsluga extends BroadcastReceiver {

    // public static final int NOTIFICATION_ID = 5453;
	//tst mnjnjnj
    private final AtomicInteger c = new AtomicInteger(0);
    protected Obserwator obserwator;
    protected ArrayAdapter<String> adapter;
    private DBHelper dbhelper;
    public static String NOTIFICATION_ID = "notification-id";
    public static String NOTIFICATION = "notification";

    public void onReceive(Context context, Intent intent) {
        Obserwator obserwator = new Obserwator();
        dbhelper = new DBHelper(context);
        obserwator.importUrls(dbhelper);
        for (String url : obserwator.zwrocUrle()) {
            Log.i("URL",url);
            GetterContentUrl mAuthTask = new GetterContentUrl(url);
            try {

                String response = mAuthTask.execute().get();
                Log.i("PAGE", response);
                //if(isNetworkAvailable(context) || ! (response.isEmpty())){
                if( ! (response.isEmpty())){
                    Log.d("Net Available ? "," stat:= "+!response.isEmpty()+"\n");
                    try
                    {
                        String odpowiedz=Pobieracz.sprawdz_zmiana(context,url,response,dbhelper);

                        if(odpowiedz.contains("tak")) send_notification(context,url);
                    }
                    catch (NoSuchAlgorithmException e)
                    {
                        e.printStackTrace();
                    }

                }
                else
                {
                    Log.d("Net not Available  "," stat:= "+!response.isEmpty()+"\n");
                }
            } catch (IOException e) {
                Log.i("TRY",e.getMessage());
            } catch (OutOfMemoryError e) {
                Log.i("TRY",e.getMessage());
            } catch (InterruptedException e) {
                Log.i("TRY",e.getMessage());
            } catch (ExecutionException e) {
                Log.i("TRY",e.getMessage());
            }
        }
    }

     /*
        Wysyłanie powiadomienia o zmianach w linku
        @author: Michał Hudaszek
     */

    public void send_notification(Context context,String url){
        if(!url.contains("http")){
            url="http://"+url;
        }
        Log.i("PARSOWANY URL",Uri.parse(url).toString());
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context.getApplicationContext());
        stackBuilder.addParentStack(ManageLinksActivity.class);
        stackBuilder.addNextIntent(browserIntent);
        PendingIntent pending = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new Notification.Builder(context.getApplicationContext())
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentTitle("Dokonano zmian w linku")
                .setAutoCancel(true)
                .setWhen(0)
                .setContentIntent(pending)
                .setContentText(url)
                .build();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(c.incrementAndGet(), notification);
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public boolean hasActiveInternetConnection(Context context) {
        if (isNetworkAvailable(context)) {
            try {
                HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                urlc.setRequestProperty("User-Agent", "Test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1500);
                urlc.connect();
                return (urlc.getResponseCode() == 200);
            } catch (IOException e) {
                Log.e("E", "Error checking internet connection", e);
            }
        } else {
            Log.d("E", "No network available!");
        }
        return false;
    }

    public class GetterContentUrl extends AsyncTask<String, Integer, String> {

        private String Content;
        private String Error = null;
        private String url;
        private String _response;
        private Context context;

        GetterContentUrl(String url) {
            this.url=url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {}

        @Override
        protected String doInBackground(String... c)  {
            this._response="";
            try {
                HttpClient httpclient = new DefaultHttpClient();
                if(!(this.url.contains("http://")) && !(this.url.contains("https://")) )
                {
                    this.url="http://"+this.url;
                }
                Log.i("Httpget link: ",this.url);

                HttpGet httpget = new HttpGet(this.url);

                HttpResponse response = httpclient.execute(httpget);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line="";
                while ((line = reader.readLine()) != null)
                    sb.append(line + "\n");

                String resString = sb.toString();
                is.close();
                String _response= resString;
                this._response=_response;
                return this._response;
            } catch (Exception e) {
                Log.i("UWAGA!", "Błąd: " + e.getMessage());
            }
            return this._response;
        }

        @Override
        protected void onPostExecute(String a) { }
    }
}
