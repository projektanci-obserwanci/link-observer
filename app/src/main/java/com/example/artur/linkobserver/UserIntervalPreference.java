package com.example.artur.linkobserver;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.NumberPicker;
import android.widget.Switch;
import android.widget.TextView;

public class UserIntervalPreference extends android.preference.DialogPreference {

    private static final int DEFAULT_MAX_VALUE = 59;
    private static final int DEFAULT_MIN_VALUE = 0;
    private static final boolean DEFAULT_WRAP_SELECTOR_WHEEL = true;

    private final int minValue;
    private final int maxValue;
    private final boolean wrapSelectorWheel;

    private Switch sw;
    private TextView tv1;
    private TextView tv2;
    private TextView tv3;
    private TextView tvs;
    private NumberPicker picker1;
    private NumberPicker picker2;
    private NumberPicker picker3;
    private int hours;
    private int minutes;
    private int seconds;
    private boolean switcher;

    public UserIntervalPreference(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.dialogPreferenceStyle);

        SharedPreferences sharedPref = this.getContext().getSharedPreferences("user-preferences",Context.MODE_PRIVATE);
        int hour=sharedPref.getInt(Integer.toString(R.string.hour_title_times),1);
        int minute=sharedPref.getInt(Integer.toString(R.string.minute_title_times),1);
        int second=sharedPref.getInt(Integer.toString(R.string.second_title_times),1);
        boolean checked_switch=sharedPref.getBoolean(Integer.toString(R.string.switch_title_times),false);

        setHour(hour);
        setMinute(minute);
        setSecond(second);
        setSwitch(checked_switch);
    }

    public UserIntervalPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.UserIntervalPreference);
        minValue = a.getInteger(R.styleable.UserIntervalPreference_minValue, DEFAULT_MIN_VALUE);
        maxValue = a.getInteger(R.styleable.UserIntervalPreference_maxValue, DEFAULT_MAX_VALUE);
        wrapSelectorWheel = a.getBoolean(R.styleable.UserIntervalPreference_wrapSelectorWheel, DEFAULT_WRAP_SELECTOR_WHEEL);
        a.recycle();
    }

    @Override
    protected View onCreateDialogView() {
        FrameLayout.LayoutParams layoutParams1 = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams1.gravity = Gravity.START;
        layoutParams1.topMargin=105;
        layoutParams1.leftMargin=24;    //numberpicker

        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams2.gravity = Gravity.CENTER_HORIZONTAL;
        //layoutParams2.bottomMargin=8;
        layoutParams2.topMargin = 105;   //numberpicker

        FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams3.gravity = Gravity.END;
        layoutParams3.topMargin=105;
        //layoutParams3.topMargin = 60;
        layoutParams3.rightMargin=24;   //numberpicker

        FrameLayout.LayoutParams layoutParams4 = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams4.gravity = Gravity.START;
        //layoutParams4.topMargin=24;
        layoutParams4.topMargin = 100;
        layoutParams4.leftMargin=34;        //tv tv1

        FrameLayout.LayoutParams layoutParams5 = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //layoutParams5.topMargin=24;
        layoutParams5.topMargin=100;
        layoutParams5.gravity = Gravity.CENTER_HORIZONTAL;      //tv tv2

        FrameLayout.LayoutParams layoutParams6 = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //layoutParams6.topMargin=24;
        layoutParams6.topMargin=100;
        layoutParams6.gravity = Gravity.END;
        layoutParams6.rightMargin=34;   //tv tv3

        FrameLayout.LayoutParams layoutParams7 = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams7.leftMargin=34;
        layoutParams7.topMargin=670;
        //layoutParams7.bottomMargin=370;
        layoutParams7.gravity = Gravity.START;  //tv 'uzyj interwalu'

        FrameLayout.LayoutParams layoutParams8 = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams8.topMargin=670;
        layoutParams8.gravity = Gravity.END;
        layoutParams8.rightMargin=34;   //sw


        picker1 = new NumberPicker(getContext());
        picker1.setLayoutParams(layoutParams1);

        tv1=new TextView(getContext());
        tv1.setText(R.string.hour_title_times);
        tv1.setLayoutParams(layoutParams4);

        picker2 = new NumberPicker(getContext());
        picker2.setLayoutParams(layoutParams2);

        tv2=new TextView(getContext());
        tv2.setText(R.string.minute_title_times);
        tv2.setLayoutParams(layoutParams5);

        picker3 = new NumberPicker(getContext());
        picker3.setLayoutParams(layoutParams3);

        tv3=new TextView(getContext());
        tv3.setText(R.string.second_title_times);
        tv3.setLayoutParams(layoutParams6);

        tvs=new TextView(getContext());
        tvs.setText(R.string.user_title_times_on_off);
        tvs.setLayoutParams(layoutParams7);

        sw=new Switch(getContext());
        sw.setId(R.id.switcher_user);
        sw.setLayoutParams(layoutParams8);

        FrameLayout dialogView = new FrameLayout(getContext());

        dialogView.addView(tv1);
        dialogView.addView(tv2);
        dialogView.addView(tv3);

        dialogView.addView(picker1);
        dialogView.addView(picker2);
        dialogView.addView(picker3);

        dialogView.addView(tvs);
        dialogView.addView(sw);

        return dialogView;
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        picker1.setMinValue(minValue);
        picker1.setMaxValue(maxValue);
        picker1.setWrapSelectorWheel(wrapSelectorWheel);
        picker1.setValue(getHour());

        picker2.setMinValue(minValue);
        picker2.setMaxValue(maxValue);
        picker2.setWrapSelectorWheel(wrapSelectorWheel);
        picker2.setValue(getMinute());

        picker3.setMinValue(minValue);
        picker3.setMaxValue(maxValue);
        picker3.setWrapSelectorWheel(wrapSelectorWheel);
        picker3.setValue(getSecond());

        sw.setChecked(getChecked());
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            picker1.clearFocus();
            picker2.clearFocus();
            picker3.clearFocus();
            sw.clearFocus();
            int newValue1 = picker1.getValue();
            int newValue2 = picker2.getValue();
            int newValue3 = picker3.getValue();
            boolean sw_value=sw.isChecked();
            SharedPreferences.Editor preferencesEditor = this.getContext().getSharedPreferences("user-preferences",Context.MODE_PRIVATE).edit();

            if (callChangeListener(newValue1)) {
                setHour(newValue1);
                preferencesEditor.putInt(Integer.toString(R.string.hour_title_times), newValue1);
            }
            if (callChangeListener(newValue2)) {
                setMinute(newValue2);
                preferencesEditor.putInt(Integer.toString(R.string.minute_title_times), newValue2);
            }
            if (callChangeListener(newValue3)) {
                setSecond(newValue3);
                preferencesEditor.putInt(Integer.toString(R.string.second_title_times), newValue3);
            }
            if(callChangeListener(sw_value)){
                setSwitch(sw_value);
                preferencesEditor.putBoolean(Integer.toString(R.string.switch_title_times), sw_value);
            }
            preferencesEditor.apply();

        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getInt(index, minValue);
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
        SharedPreferences sharedPref = this.getContext().getSharedPreferences("user-preferences",Context.MODE_PRIVATE);
        int hour=sharedPref.getInt(Integer.toString(R.string.hour_title_times),1);
        int minute=sharedPref.getInt(Integer.toString(R.string.minute_title_times),1);
        int second=sharedPref.getInt(Integer.toString(R.string.second_title_times),1);
        boolean checked_switch=sharedPref.getBoolean(Integer.toString(R.string.switch_title_times),true);

        setHour(hour);
        setMinute(minute);
        setSecond(second);
        setSwitch(checked_switch);
    }

    private void setSwitch(boolean checked) {
        this.switcher=checked;
    }

    private void setHour(int value) {
        this.hours = value;
        persistInt(this.hours);
    }

    private void setMinute(int value) {
        this.minutes = value;
        persistInt(this.minutes);
    }

    private void setSecond(int value) {
        this.seconds = value;
        persistInt(this.seconds);
    }

    private boolean getChecked() {
        return this.switcher?this.switcher:false;
    }

    private int getHour() {
        return this.hours;
    }

    private int getMinute() {
        return this.minutes;
    }

    private int getSecond() {
        return this.seconds;
    }
}
