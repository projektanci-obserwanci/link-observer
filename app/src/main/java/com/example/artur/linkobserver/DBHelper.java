package com.example.artur.linkobserver;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper
{
    public static final int DB_VERSION=3;
    private static final String DB_NAME = "LinObserver.db";
    private static final String TABLE_LINKS_NAME = "Links";
    private static final String ID_COLUMN_NAME="id";
    private static final String URL_COLUMN_NAME="url";
    private static final String HASH_COLUMN_NAME="hash";

    public DBHelper(Context context)
    {
        super(context, DB_NAME, null, DB_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String cmd_sql = "CREATE TABLE " + TABLE_LINKS_NAME + " ( " +
                ID_COLUMN_NAME + " INTEGER PRIMARY KEY," +
                URL_COLUMN_NAME + " TEXT," +
                HASH_COLUMN_NAME + " TEXT" + ")";


        db.execSQL(cmd_sql);
        Log.d("Create" ,"Pomyślnie utworzono tabelę links");
        Log.d("List" ,"cmdsql:\n"+cmd_sql);
    }
    public ArrayList<String> getAllRows()
    {
        SQLiteDatabase db=this.getReadableDatabase();
        ArrayList <String> tmp=new ArrayList<>();
        Cursor kursor=db.rawQuery("select "+ID_COLUMN_NAME+","+URL_COLUMN_NAME+","+HASH_COLUMN_NAME+" from links",null);
        if(kursor.moveToFirst())
        {
            do
            {
                tmp.add(kursor.getString(0));

            }while(kursor.moveToNext());

        }
        kursor.close();
        return tmp;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LINKS_NAME);
        onCreate(db);
    }
    public void addLink(String url)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(URL_COLUMN_NAME, url);
        db.insert(TABLE_LINKS_NAME, null, contentValues);
    }
    public void addHash(String hash)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(HASH_COLUMN_NAME, hash);
        db.insert(TABLE_LINKS_NAME, null, contentValues);
    }
    public String getHash(String url)
    {
        SQLiteDatabase db=this.getReadableDatabase();
        String tmp=new String();
        Cursor kursor=db.rawQuery("select "+HASH_COLUMN_NAME+" from links where url like '"+url+"' ",null);
        Log.d("getHash","select "+HASH_COLUMN_NAME+" from links where url like '"+url+"' ");
        if(kursor.getCount()>0)
        {
            kursor.moveToFirst();
            tmp= kursor.getString(kursor.getColumnIndex(HASH_COLUMN_NAME));
        }
        kursor.close();
        return tmp;
    }
    public void updateHash(String hash,String url)
    {
        Log.d("upd_hash_arg",hash+" dla url: \n"+url);
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(HASH_COLUMN_NAME, hash);
        String whereClause=URL_COLUMN_NAME+"='"+url+"'";

        Log.d("DB-arg","cont_val:\n"+contentValues.toString()+"\nwhere_claus:\n"+whereClause);
        db.update(TABLE_LINKS_NAME,contentValues,whereClause,null);

    }

    public void deleteLink(String url)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_LINKS_NAME, URL_COLUMN_NAME + "=" + "'"+url+ "'", null);
    }

    public ArrayList<String> getUrls() {
        SQLiteDatabase db=this.getReadableDatabase();
        ArrayList <String> tmp=new ArrayList<>();
        Cursor kursor=db.rawQuery("select "+URL_COLUMN_NAME+" from links",null);
        if(kursor.moveToFirst())
        {
            do
            {
                tmp.add(kursor.getString(0));

            }while(kursor.moveToNext());

        }
        kursor.close();
        return tmp;
    }

    public void clearTable()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String cmd_sql="DELETE FROM "+TABLE_LINKS_NAME;
        db.execSQL(cmd_sql);
    }


}
