package com.example.artur.linkobserver;

import android.app.Activity;
import android.app.Dialog;
import android.app.Instrumentation;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Debug;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SmallTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.app.AlertDialog;
import android.test.ActivityUnitTestCase;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.WeakHashMap;
public class DodawanieLinkowInstrumentedTest extends ActivityUnitTestCase<MainActivity> {

    private MainActivity activity;
    private Button manageLinksBtn, addLinkBtn, deletebtn;
    private EditText podaj_url;
    private ListView lv;
    private Context context;

    private String url="http://interia.pl";

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    public DodawanieLinkowInstrumentedTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception{
        super.setUp();

        Context appContext = InstrumentationRegistry.getTargetContext();
        this.context=appContext.getApplicationContext();
        Intent intent = new Intent(this.context, MainActivity.class);
        mActivityTestRule.launchActivity(intent);

        activity = mActivityTestRule.getActivity();
        manageLinksBtn = (Button) activity.findViewById(R.id.manageLinksBtn);

    }

    @SmallTest
    public void testPowinnoPrzejśćDoZarządzaniaLinkamiIPokazaćJakDodaćLink() throws Throwable,SecurityException,InterruptedException {

        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(ManageLinksActivity.class.getName(), null, false);

        Thread.sleep(4000);
        manageLinksBtn.callOnClick();
        Thread.sleep(4000);

        final Activity nextActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 4000);

        podaj_url = (EditText) nextActivity.findViewById(R.id.et);
        addLinkBtn= (Button) nextActivity.findViewById(R.id.addLinkBtn);

        Thread.sleep(4000);

        mActivityTestRule.runOnUiThread(new Runnable()  {
            @Override
            public void run()  {
                podaj_url.setText(url);
            }
        });
        Thread.sleep(4000);
        mActivityTestRule.runOnUiThread(new Runnable()  {
            @Override
            public void run()  {
                addLinkBtn.performClick();
            }
        });
        Thread.sleep(3000);
        assertTrue(true);
    }
}