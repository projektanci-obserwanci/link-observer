package com.example.artur.linkobserver;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.filters.SmallTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.support.v7.widget.SwitchCompat;
import android.test.ActivityUnitTestCase;
import android.view.View;
import android.widget.Button;
import android.widget.Checkable;
import android.widget.ListView;
import android.widget.Switch;


import org.hamcrest.BaseMatcher;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.Description;
import org.junit.runner.RunWith;

import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isChecked;
import static android.support.test.espresso.matcher.ViewMatchers.isNotChecked;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static android.support.test.uiautomator.By.checkable;
import static android.support.test.uiautomator.By.checked;
import static android.support.test.uiautomator.By.text;


public class PowiadomienieUzytkownikaInstrumentedTest extends ActivityUnitTestCase<MainActivity> {

    private MainActivity activity;
    private ListView lv;
    private Context context;
    private Button manageLinksBtn, settingsBtn;
    private Switch switch_button;

    private String NOTIFICATION_TITLE="Dokonano zmian w linku";

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    public PowiadomienieUzytkownikaInstrumentedTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception{
        super.setUp();
        Context appContext = InstrumentationRegistry.getTargetContext();
        this.context=appContext.getApplicationContext();//this.getInstrumentation().getContext();
        Intent intent2 = new Intent(this.context, MainActivity.class);
        mActivityTestRule.launchActivity(intent2);

        activity=mActivityTestRule.getActivity();
        manageLinksBtn = (Button) activity.findViewById(R.id.manageLinksBtn);
        activity=mActivityTestRule.getActivity();
    }

    @SmallTest
    public void testPowinnoPrzyjscPowiadomienieUzytkownika() throws Throwable,SecurityException,InterruptedException {

        Instrumentation.ActivityMonitor activityMonitor2 = getInstrumentation().addMonitor(SettingsActivity.class.getName(), null, false);

        activity=mActivityTestRule.getActivity();
        settingsBtn = (Button) activity.findViewById(R.id.settingsBtn);
        Thread.sleep(4000);
        settingsBtn.callOnClick();
        Thread.sleep(4000);
        Espresso.onView(withText(R.string.user_title_times)).perform(scrollTo()).perform(click());
        Thread.sleep(2000);

        Espresso.onView(withId(R.id.switcher_user)).perform(scrollTo()).perform(click());

        Thread.sleep(2000);
        Espresso.onView(withText("OK")).perform(scrollTo()).perform(click());

        final Activity nextActivity2 = getInstrumentation().waitForMonitorWithTimeout(activityMonitor2, 4000);

        Thread.sleep(4000);
        mActivityTestRule.runOnUiThread(new Runnable()  {
            @Override
            public void run() {
                nextActivity2.onBackPressed();
            }
        });
        Instrumentation.ActivityMonitor activityMonitor3 = getInstrumentation().addMonitor(ManageLinksActivity.class.getName(), null, false);

        Thread.sleep(4000);
        manageLinksBtn.callOnClick();
        Thread.sleep(4000);

        final Activity nextActivity3 = getInstrumentation().waitForMonitorWithTimeout(activityMonitor3, 4000);

        switch_button = (Switch) nextActivity3.findViewById(R.id.obserwe_on_off);
        lv = (ListView) nextActivity3.findViewById(R.id.linksListView);

        if(lv.getAdapter().getCount()!=0){
            Thread.sleep(4000);
            mActivityTestRule.runOnUiThread(new Runnable()  {
                @Override
                public void run()  {
                    switch_button.setChecked(true);//.performClick();
                }
            });
            Thread.sleep(5000);
            UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
            device.pressHome();
            Thread.sleep(65000);

            device.openNotification();
            device.wait(Until.hasObject(text(NOTIFICATION_TITLE)), 4000);
            UiObject title = device.findObject(new UiSelector().text(NOTIFICATION_TITLE));
            Thread.sleep(4000);
            assertEquals(true,title.exists());
            if(title.exists() ){
                try {
                    title.click();
                    Thread.sleep(8000);
                    assertTrue(true);
                } catch (UiObjectNotFoundException e){
                    assertTrue(false);
                }
            } else {
                assertTrue(false);
            }
        } else {
            assertNull(lv.getAdapter().getItem(0));
        }
    }
}
