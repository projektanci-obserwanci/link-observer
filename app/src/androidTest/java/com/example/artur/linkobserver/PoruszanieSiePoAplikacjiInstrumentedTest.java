package com.example.artur.linkobserver;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.filters.SmallTest;
import android.support.test.rule.ActivityTestRule;
import android.test.ActivityUnitTestCase;
import android.widget.Button;

import org.junit.Rule;

import java.util.List;

import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.isChecked;
import static android.support.test.espresso.matcher.ViewMatchers.isNotChecked;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

public class PoruszanieSiePoAplikacjiInstrumentedTest extends ActivityUnitTestCase<MainActivity> {

    private MainActivity activity;
    private Activity activity2;
    private Button manageLinksBtn, settingsBtn;
    private Context context;
    private static SettingsActivity.MyPreferenceFragment fragment;

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    public PoruszanieSiePoAplikacjiInstrumentedTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception{
        super.setUp();
        Context appContext = InstrumentationRegistry.getTargetContext();
        this.context=appContext.getApplicationContext();//this.getInstrumentation().getContext();
        Intent intent = new Intent(this.context, MainActivity.class);
        mActivityTestRule.launchActivity(intent);

        activity = mActivityTestRule.getActivity();
        manageLinksBtn = (Button) activity.findViewById(R.id.manageLinksBtn);
        settingsBtn = (Button) activity.findViewById(R.id.settingsBtn);
    }

    @SmallTest
    public void testPowinnoPrzejscZMenuDoZarządzaniaLinkami() throws Throwable,NullPointerException,SecurityException,InterruptedException {

        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(ManageLinksActivity.class.getName(), null, false);

        Thread.sleep(4000);
        manageLinksBtn.callOnClick();
        Thread.sleep(4000);

        final Activity nextActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 4000);

        mActivityTestRule.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                nextActivity.onBackPressed();
            }
        });
 }

    @SmallTest
    public void testPowinnoPrzejscZMenuDoZarządzaniaUstawieniami() throws Throwable,NullPointerException,SecurityException,InterruptedException {

        Instrumentation.ActivityMonitor activityMonitor2 = getInstrumentation().addMonitor(SettingsActivity.class.getName(), null, false);

        Thread.sleep(4000);
        settingsBtn.callOnClick();
        Thread.sleep(4000);
        Espresso.onView(withText(R.string.title_times)).perform(scrollTo()).perform(click());
        Thread.sleep(4000);
        //Espresso.onView(withText("CANCEL")).perform(scrollTo()).perform(click());
        Espresso.pressBack();

        Thread.sleep(4000);
        Espresso.onView(withText(R.string.user_title_times)).perform(scrollTo()).perform(click());
        Thread.sleep(4000);
        //Espresso.onView(withText("CANCEL")).perform(scrollTo()).perform(click());
        Espresso.pressBack();

        final Activity nextActivity2 = getInstrumentation().waitForMonitorWithTimeout(activityMonitor2, 4000);

        Thread.sleep(4000);
        mActivityTestRule.runOnUiThread(new Runnable()  {
            @Override
            public void run() {
                nextActivity2.onBackPressed();
            }
        });
        Thread.sleep(4000);
        assertTrue(true);
    }
}