package com.example.artur.linkobserver;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.filters.SmallTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.app.AlertDialog;
import android.test.ActivityUnitTestCase;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public class UsuwanieLinkowInstrumentedTest extends ActivityUnitTestCase<MainActivity> {

    private MainActivity activity;
    private Button manageLinksBtn, addLinkBtn, deletebtn;
    private ListView lv;
    private Context context;

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    public UsuwanieLinkowInstrumentedTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception{
        super.setUp();
        Context appContext = InstrumentationRegistry.getTargetContext();
        this.context=appContext.getApplicationContext();//this.getInstrumentation().getContext();
        Intent intent = new Intent(this.context, MainActivity.class);
        mActivityTestRule.launchActivity(intent);

        activity = mActivityTestRule.getActivity();
        manageLinksBtn = (Button) activity.findViewById(R.id.manageLinksBtn);
    }

    @Test
    public void testPowinnoPrzejśćDoZarządzaniaLinkamiIPokazaćJakUsunąćLink() throws Throwable,NullPointerException,SecurityException,InterruptedException {

        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(ManageLinksActivity.class.getName(), null, false);

        Thread.sleep(4000);
        manageLinksBtn.callOnClick();
        Thread.sleep(4000);

        final Activity nextActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 4000);

        Thread.sleep(4000);
        lv = (ListView) nextActivity.findViewById(R.id.linksListView);
        mActivityTestRule.runOnUiThread(new Runnable()  {
            @Override
            public void run()  {
                lv.performItemClick(lv.getSelectedView(),0,Long.parseLong("25"));
            }
        });
        Thread.sleep(3000);
        Espresso.onView(withId(R.id.btndialoglink)).perform(scrollTo()).perform(click());
        Thread.sleep(3000);
        assertTrue(true);
    }
}